import $ from "jquery";
import _ from "underscore";

class ElementsFactory{

    static createInput(element, className){
        className = className ? className : "input-data";
        
        let wrapper = ElementsFactory.createWrapper(element, className);
        let textInput = $(`
            <input type="text" 
                class="data-source" 
                value="${element.query}">
            </input>
        `);
        
        wrapper.find("." + className).append(textInput);
        return wrapper;
    }

    static createSelect(element, className){
        element.id = !_.isUndefined(element.id) ? element.id : 0;
        className = className ? className : "select-values";

        let wrapper = ElementsFactory.createWrapper(element, className);
        
        let select = $(`
            <select id="${element.elementKey}"
                class="data-source"">
            </select>
        `);
        _.forEach(element.defaultsValues,function(option){
            select.append("<option value=\"" + option.id + "\">" + option.translate + "</option>");
        });
        
        wrapper.find("." + className).append(select);
        return wrapper;
    }

    static createRadioButtonsGroup(element, className){
        className = !_.isUndefined(className) ? className : "radio-group";
        
        let wrapper = ElementsFactory.createWrapper(element, className, "menu-radio-group");
        _.forEach(element.defaultsValues, function(value, index){
            let radioButton = $(`
                <div class="radio-${index}">
                    <input type="radio"
                        class="data-source"
                        value="${value.id}" 
                        name="${element.elementKey}"
                        ` + (element.id == value.id ? "checked" : "") + `>
                        <span translate="${value.translateKey}">${value.translate}</span>
                    </input>
                </div>`
            );
            wrapper.find("." + className).append(radioButton);
        });
        return wrapper;
    }

    static createWrapper(element, className, secondClass){
        if(_.isUndefined(secondClass))
            secondClass = "";
        return $(`
            <div class="${className}-wrapper" id="${element.elementKey}">
                <span class="${className}-text" translate="${element.translateKey}">${element.translatedField}</span>
                <div class="${className} ${secondClass}"></div>
            </div>`
        );
    }

    static createFabIcon(){
        return $(`
            <div class="fab-toggle-menu">
                <div class="icons">
                    <i class="fas fa-bars"></i>
                    <i class="fas fa-eye"></i>
                    <i class="fas fa-eye-slash"></i>
                </div>
            </div>
        `)
    }
}
export default ElementsFactory;