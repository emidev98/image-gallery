import _ from 'underscore';
import $ from 'jquery';
import Image from './../models/Image';


class HttpService{

    constructor(){
        this.httpConfig = require("./../../../api.config.json");
        this.mainUrl = "https://pixabay.com/api?key=" + this.httpConfig.apiKey;
        this.apiUrl = this.mainUrl;
        this.imageList = [];
    }
    
    getGalleryList(pageNumber, resetImagesList) {
        console.log("pageNumber",pageNumber)
        console.log("resetImagesList",resetImagesList)
        console.log("apiUrl", this.apiUrl)
        var self = this;
        if(_.isUndefined(pageNumber)) pageNumber = 1;
        return $.get(this.apiUrl + "&per_page=40&page=" + pageNumber).then(function(data){
            if(resetImagesList) self.imageList = [];
            _.forEach(data.hits,function(hit){
                let image = new Image();
                image = _.extend(image, hit);
                self.imageList.push(image);
            });
            return self.imageList;
        });
    }

    buildApiUrl(galleryMenu){
        let url = "";
        this.apiUrl = "";
        _.forEach(galleryMenu,function(_prop){
            url += _prop.getStringUrlProperty();
        });
        this.apiUrl += this.mainUrl + url.toLowerCase();
    }
}

export default HttpService;