import $ from "jquery";
import _ from "underscore";
import download from "downloadjs";

class ImageDetailedView{
    
    constructor(image,galleryView) {
        this.image = image;
        this.galleryView = galleryView;
        this.imageElement = this.createImageElement();
        this.showElement();
    }

    createImageElement(){
        var self = this;
        var imgElement = $(`
            <div class="image-detailed-view-wrapper">
                <div class="image-detailed-view">
                    <header>
                        <div class="icon-wrapper action-button navigate-to-previous-image">
                            <i class="fas fa-arrow-left"/>
                        </div>
                        <div class="header-center">
                            <div class="icon-wrapper action-button navigate-to-user-page">
                                <i class="fas fa-user"/>
                                <span class="additional-info">
                                    ${self.image.user}
                                </span>
                            </div>
                            <div class="icon-wrapper action-button navigate-to-main-page">
                                <i class="fas fa-eye"/>
                                <span class="additional-info">
                                    ${self.image.views}
                                </span>
                                </div>
                            <div class="icon-wrapper action-button download-image">
                                <i class="fas fa-download"/>
                                <span class="additional-info">
                                    ${self.image.downloads}
                                </span>
                            </div>
                            <div class="icon-wrapper">
                                <i class="fas fa-heart"/>
                                <span class="additional-info">
                                    ${self.image.likes}
                                </span>
                            </div>
                            <div class="icon-wrapper">
                                <i class="fas fa-comment"/>
                                <span class="additional-info">
                                    ${self.image.comments}
                                </span>
                            </div>
                            <div class="icon-wrapper action-button exit-detail-view">
                                <i class="fas fa-times"/>
                            </div>
                        </div>
                        <div class="icon-wrapper action-button navigate-to-next-image">
                            <i class="fas fa-arrow-right"/>
                        </div>
                    </header>
                    <content>
                        <img class="detailed-image" src="${self.image.largeImageURL}"/>
                    </content>
                </div>
            </div>
        `);
        return imgElement;
    }

    showElement(){
        var self = this;
        self.imageElement.appendTo("body");
        setTimeout(function(){
            self.imageElement.addClass("show-detailed-view");
            self.addEventListeners();
        },250);
    }

    destroyElement(){
        var self = this;
        self.imageElement.removeClass("show-detailed-view");
        setTimeout(function(){
            self.imageElement.remove();
        },500);
    }

    addEventListeners(){
        var self = this;

        self.imageElement.find("div.image-detailed-view").on("click",function(event){
            if(event.target.className == "image-detailed-view" ||
               event.target.tagName == "HEADER"){
                self.destroyElement();
            }
        });

        self.imageElement.find(".navigate-to-previous-image").on("click",function(){
            self.getNextImageBy(-1);
        });

        self.imageElement.find(".navigate-to-user-page").on("click",function(){
            let userProfileUrl = "https://pixabay.com/en/users/" + self.image.user + "-" + self.image.user_id;
            window.open(userProfileUrl,"_blank")

        });

        self.imageElement.find(".navigate-to-main-page").on("click",function(){
            window.open(self.image.pageURL,"_blank")
        });

        self.imageElement.find(".download-image").on("click",function(){
            download(self.image.largeImageURL);
        });
        
        self.imageElement.find(".exit-detail-view").on("click",function(){
            self.destroyElement();
        });

        self.imageElement.find(".navigate-to-next-image").on("click",function(){
            self.getNextImageBy(1);
        });
    }

    getNextImageBy(nextPosition){
        let self = this;
        let currentImageIndex = _.chain(self.galleryView.httpService.imageList)
            .pluck("id")
            .findIndex(function(id){
                return id == self.image.id;
            })
            .value();
        let nextImage = self.galleryView.httpService.imageList[currentImageIndex + ( nextPosition )];
        if(_.isUndefined(nextImage)){
            let nextPageIndex = self.galleryView.currentPageNumber + 1;
            self.galleryView.addMoreImagesToGallery(nextPageIndex,false).then(function(){
                self.getNextImageBy(0);
                // TODO add loader
            });
        }
        else self.renderNextImage(nextImage);
    }

    renderNextImage(image){
        let self = this;
        self.image = image;
        self.imageElement.find("img").attr("src",self.image.largeImageURL);
        self.imageElement.find("header").remove();
        self.imageElement.find(".image-detailed-view").prepend(`
            <header>
                <div class="icon-wrapper action-button navigate-to-previous-image">
                    <i class="fas fa-arrow-left"/>
                </div>
                <div class="header-center">
                    <div class="icon-wrapper action-button navigate-to-user-page">
                        <i class="fas fa-user"/>
                        <span class="additional-info">
                            ${self.image.user}
                        </span>
                    </div>
                    <div class="icon-wrapper action-button navigate-to-main-page">
                        <i class="fas fa-eye"/>
                        <span class="additional-info">
                            ${self.image.views}
                        </span>
                        </div>
                    <div class="icon-wrapper action-button download-image">
                        <i class="fas fa-download"/>
                        <span class="additional-info">
                            ${self.image.downloads}
                        </span>
                    </div>
                    <div class="icon-wrapper">
                        <i class="fas fa-heart"/>
                        <span class="additional-info">
                            ${self.image.likes}
                        </span>
                    </div>
                    <div class="icon-wrapper">
                        <i class="fas fa-comment"/>
                        <span class="additional-info">
                            ${self.image.comments}
                        </span>
                    </div>
                    <div class="icon-wrapper action-button exit-detail-view">
                        <i class="fas fa-times"/>
                    </div>
                </div>
                <div class="icon-wrapper action-button navigate-to-next-image">
                    <i class="fas fa-arrow-right"/>
                </div>
            </header>
        `);
        self.addEventListeners();
    }
}

export default ImageDetailedView;