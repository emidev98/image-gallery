import $ from "jquery";
import _ from "underscore";
import download from "downloadjs";
import ImageDetailedView from "./ImageDetailedView";

class ImageThumbnailView{
    
    constructor(image,galleryView){
        this.galleryView = galleryView;
        this.image = image;
        this.imageElement = this.createElement(image);
        this.setOnClickListeners();
    }

    createElement(image){
        if(_.isUndefined(this.imageElement)){
            let divWrapper = $(`
                <div id="${image.id}" class="image-thumbnail">
                    <div class="image-wrapper">
                        <img src="${image.previewURL}"></img>
                    </div>
                    <div class="image-info">
                        <div class="user">
                            <td><i class="far fa-user"></i></td>
                            <td>${image.user}</td>
                        </div>
                        <table>
                            <tr>
                                <td><i class="far fa-eye"></i></td>
                                <td>${image.views}</td>
                                <td><i class="fas fa-download"></i></td>
                                <td>${image.downloads}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            `);

            return divWrapper;
        }
        else return this.imageElement;
    }
    
    setOnClickListeners(){
        let self = this;

        self.imageElement.find(".image-wrapper").on("click",function(){
            self.imageDetailedView = new ImageDetailedView(self.image, self.galleryView);
        });

        self.imageElement.find(".fa-download").on("click",function(){
            download(self.image.largeImageURL);
        });

        self.imageElement.find(".fa-eye").on("click",function(){
            window.open(self.image.pageURL,"_blank")
        });
    }
}

export default ImageThumbnailView;