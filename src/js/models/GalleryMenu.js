import Query from './Query';
import GalleryMenuFilterModel from './GalleryMenuFilterModel';
import Size from './Size';

class GalleryMenu{
    
    constructor(){
        this.query = new Query("","search");
        this.size =  new Size(0,"size");
        this.orientation = new GalleryMenuFilterModel(0,"orientation");
        this.category = new GalleryMenuFilterModel(0,"category");
        this.lang = new GalleryMenuFilterModel(0,"lang");
        this.image_type = new GalleryMenuFilterModel(0,"image_type");
        this.color = new GalleryMenuFilterModel(0,"colors");
        this.order = new GalleryMenuFilterModel(0,"order");
        this.editors_choice = new GalleryMenuFilterModel(0,"editors_choice");
    }
}

export default GalleryMenu;