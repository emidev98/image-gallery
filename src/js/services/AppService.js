import TranslateService from "./TranslateService";
import GalleryMenuView from "./../views/GalleryMenuView";
import GalleryView from "./../views/gallery-view/GalleryView";

class AppService{

    constructor(){
        this.translateService = new TranslateService();
        this.translateService.translateAppConfig();
        this.galleryMenuView = new GalleryMenuView();
        this.galleryView = new GalleryView(this.galleryMenuView);
    }

    init(){
        var self = this;
        self.galleryMenuView.generateElements();
        self.galleryMenuView.appendElements();
        
        self.galleryMenuView.addListeners(function(){
            console.log("galleryMenuView");
            self.galleryView.clearImages();
            self.galleryView.addMoreImagesToGallery(1,true).then(()=>{
                // TODO add loader
            });
        });
    }
}

export default AppService;