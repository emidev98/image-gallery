import _ from "underscore";
import AppService from "../services/AppService";
import Utils from "../factory/Utils";
import TranslateService from "./../services/TranslateService";

class GalleryMenuFilterModel{

    constructor(id, elementKey){
        this.translateKey = "COMMON." + elementKey.toUpperCase();
        this.elementKey = elementKey;
        this.defaultsValues = AppService.appConfig[elementKey];
        this.setPropertyById(id);
        this.translateElementText();
    }

    setPropertyById(id){
        let element = Utils.getById(id, this.defaultsValues);
        this.id = element.id;
        this.urlValue = element.urlValue;
    }

    getStringUrlProperty(){
        if(_.isEmpty(this.urlValue)) return "";
        else return "&" + this.elementKey + "=" + this.urlValue;
    }

    translateElementText(){
        this.translatedField = TranslateService.getTranslate(this.translateKey);
    }
}

export default GalleryMenuFilterModel;