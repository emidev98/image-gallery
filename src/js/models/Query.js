import _ from "underscore";
import TranslateService from "./../services/TranslateService";

class Query {

    constructor(query,elementKey){
        this.translateKey = "COMMON." + elementKey.toUpperCase();
        this.elementKey = elementKey;
        this.elementIdentifier = "q";
        this.defaultsValues = "";
        if(_.isUndefined(query)){
            this.query = "";
        }
        else {
            this.query = query;
        }
        this.translateElementText();
    }

    setPropertyById(query){
        this.query = query;
    }
    
    getStringUrlProperty(){
        if(_.isUndefined(this.query) || _.isEmpty(this.query)){
            return "";
        }
        else{
            return "&" + this.elementIdentifier + "=" + this.query.replace(/" "/g,"+");
        }
    }

    translateElementText(){
        this.translatedField = TranslateService.getTranslate(this.translateKey);
    }
}

export default Query;