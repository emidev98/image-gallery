class Image{
    
    constructor(){
        this.comments          = undefined;
        this.downloads         = undefined;
        this.favorites         = undefined;
        this.id                = undefined;
        this.imageHeight       = undefined;
        this.imageSize         = undefined;
        this.imageWidth        = undefined;
        this.largeImageURL     = undefined;
        this.likes             = undefined;
        this.pageURL           = undefined;
        this.previewHeight     = undefined;
        this.previewURL        = undefined;
        this.previewWidth      = undefined;
        this.tags              = undefined;
        this.type              = undefined;
        this.user              = undefined;
        this.userImageURL      = undefined;
        this.user_id           = undefined;
        this.views             = undefined;
        this.webformatHeight   = undefined;
        this.webformatURL      = undefined;
        this.webformatWidth    = undefined;
    }

}

export default Image;