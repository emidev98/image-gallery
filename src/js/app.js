import "./../styles/styles.less";
import $ from 'jquery';
import AppService from './services/AppService';

AppService.appConfig = require("./../../app.config.json");

$(function(){
    var app = new AppService();
    app.init();
})