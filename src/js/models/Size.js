import _ from "underscore";
import Utils from "./../factory/Utils";
import GalleryMenuFilterModel from "./GalleryMenuFilterModel";

class Size extends GalleryMenuFilterModel{

    constructor(id, elementKey){
        super(id,elementKey);
        
    }

    setPropertyById(id){
        let size = Utils.getById(id,this.defaultsValues);
        this.id = size.id;
        this.name = size.name;
        this.height = size.height;
        this.width = size.width;
    }

    getStringUrlProperty(){
        let minWidth =  "&min_width=" + this.width;
        let minHeight =  "&min_height=" + this.height;
        return minWidth + minHeight;
    }
}

export default Size;